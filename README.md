# uca_m2bi_hpc

Ce projet est realise dans le cadre de l'UE Calculs parallèles et Programmation GPU du M2 de bioinformatique de l'UCA.

Ce depot Git permet le traitement de données rna-seq issus de la publication de Green, D., Tarazona, S., Ferreirós-Vidal, I., Ramírez, R. N., Schmidt, A., Reijmers, T. H., Von Saint Paul, V., Marabita, F., Rodríguez-Ubreva, J., García-Gómez, A., Carroll, T., Cooper, L., Liang, Z., Dharmalingam, G., Van Der Kloet, F., Harms, A. C., Balzano‐Nogueira, L., Lagani, V., Tsamardinos, I.,. . . Conesa, A. (2019b). STATEGra, a comprehensive multi-omics dataset of B-cell differentiation in mouse. Scientific Data, 6(1). https://doi.org/10.1038/s41597-019-0202-7 .
https://www.nature.com/articles/s41597-019-0202-7

Ce travail a etait effectue sur cluster HPC2 et les scripts utilisé sont interprete par Slurm.

L'environnement conda utilisé est disponible en .yml dans data/environment_samtoolshps.yml et je vous invite a creer une environement a partir de ce fichier pour lancer les scripts
conda env create -f environment.yml

Touts les scripts doivent etre lancer dans l'ordre car le prochain necessite le precedent pour fonctionner.

les données dans ./data de basent doivent etre presentes sous cette forme: 

.  
.	|----bowtie2 (dossier contenant l'index bowtie avec le prefix "all")  
.	|  
.|----flat (dossier contenant genomic.gff genomic.gtf)  
.	|  
.	|----rna-seq (dossier contenat les rna en _1.fastq.gz et _2.fastq.gz)  
.  

/!\ les scipts fonctionnent si l'arborescence du git est respecté (surtout pour ce qui est des dossiers de log)


Le script 1.fastqc_array.slurm va prendre les fichiers INPUT=data/rna-seq/*.fastq.gz et effectuer un controle qualitée fastqc OUTPUT=data/1.fastqc

Le script 2.trim_array.slurm va prendre les fichiers INPUT=data/rna-seq/*.fastq.gz et les trimmer OUTPUT=data/2.trim/*.fasstq.gz

Le script 3.align_array.slurm va prendre les fichiers INPUT=data/2.trim/*paired.fastq.gz etles alligner grace a l'index data/bowtie2/all  OUTPUT=data/3.align/*.mapped.sam

Le script 4.clean_array.slurm va prendre les fichiers INPUT=data/3.align/*.mapped.sam et va nettoyer les allignement OUTPUT=data/3.align/*.sorted.bam

Le script 5.quantif_array.slurm va prendre les fichiers INPUT=data/3.align/*.sorted.bam et va compter quuantifier les rna en utilisant data/flat/genomic.gtf OUTPUT=data/5.quantif/*.htseq.gtf

Le script 6.diff_array.slurm va prendre les fichiers INPUT=data/5.quantif/*.gtf et va faire la différenciation des transcrits avec dseq2  OUTPUT=/results/

Ce derniers rencontre des difficultees a etre execute sur HPC2 a causes des dépendances qui créer des problemes, il semble de nouveau marcher mais met enormement de temps a charger les dépendances. Il est donc fortement conseillé de le lancer en local ou dans un environement R possédant deja DSeq2.

Les fichiers de sorties sont donc disponibles dans results

Il est mis a disposition un script DATASUPR qui supprime l'enssemble des donnees genere.
